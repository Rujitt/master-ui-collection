import 'package:flutter/material.dart';
import 'package:master_ui_collection/masterui.dart';

class DialogView extends StatelessWidget {
  const DialogView({super.key});

  @override
  Widget build(BuildContext context) {
    void handleShowDefaultDialog() {
      showDialog(
        context: context,
        builder: (context) => MasterDialog(
          image: const NetworkImage('https://picsum.photos/200/300'),
          title: 'Confirm delete account',
          description: 'Are you sure you want to delete your account?',
          actions: [
            MasterButton(
              backgroundColor: Colors.red,
              buttonText: 'Delete',
              onPressed: () => Navigator.of(context).pop(),
            ),
            MasterButton(
              buttonText: 'Cancel',
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        ),
      );
    }

    void handleShowDialogWithoutImage() {
      showDialog(
        context: context,
        builder: (context) => const MasterDialog(
          title: 'Dialog without image',
          description: "This is a dialog without image",
        ),
      );
    }

    return SafeArea(
      child: Scaffold(
        body: SizedBox(
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MasterButton(
                buttonText: 'Default Dialog',
                onPressed: handleShowDefaultDialog,
              ),
              MasterButton(
                buttonText: 'Dialog without image',
                onPressed: handleShowDialogWithoutImage,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:master_ui_collection/masterui.dart';

class CarouselView extends StatelessWidget {
  const CarouselView({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                MasterCarousel(
                  autoPlay: true,
                  height: 200,
                  children: _buildCarouselItems(count: 5),
                ),
                const SizedBox(height: 16.0),
                MasterCarousel(
                  height: 200,
                  showControls: true,
                  indicatorProps: const CarouselIndicatorProps(
                      borderColor: Colors.black,
                      activeColor: Colors.pinkAccent,
                      indicatorHeight: 16,
                      indicatorWidth: 16,
                      selectedIndicatorHeight: 20,
                      selectedIndicatorWidth: 20),
                  children: _buildCarouselItems(count: 5),
                ),
                const SizedBox(height: 16.0),
                MasterCarousel(
                  height: 200,
                  showControls: false,
                  autoPlay: true,
                  autoPlayInterval: const Duration(seconds: 5),
                  indicatorProps: const CarouselIndicatorProps(selectedIndicatorWidth: 20),
                  children: _buildCarouselItems(count: 5),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildCarouselItems({int count = 5}) {
    return List.generate(
      count,
      (index) {
        Color randomColor = Color(
          (math.Random().nextDouble() * 0xFFFFFF).toInt(),
        ).withOpacity(1.0);
        return Container(
          height: 10,
          decoration: BoxDecoration(
            color: randomColor,
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: Center(
            child:Container(
              decoration: BoxDecoration(
                color: randomColor,
                borderRadius: BorderRadius.circular(20)
              ),
              child: Text(index.toString()),
            )
            // Text(
            //   index.toString(),
            // ),
          ),
        );
      },
    );
  }
}

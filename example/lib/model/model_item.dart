
class ModelItem {
  String name;
  String cast;

  ModelItem(this.name, this.cast);
}


var modelList = <ModelItem>[
  ModelItem("amir", "suwal"),
  ModelItem("Ajay", "Shrestha"),
  ModelItem("Sachin", "Aryal"),
  ModelItem("Santosh", "Acharaya"),
  ModelItem("Kabin", "Mdr"),
  ModelItem("Shyam", "kishwor"),
  ModelItem("Deepak", "Suwal"),
];
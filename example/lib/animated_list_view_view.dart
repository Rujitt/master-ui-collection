import 'package:example/radio_view.dart';
import 'package:flutter/material.dart';
import 'package:master_ui_collection/masterui.dart';

class AnimatedListViewView extends StatelessWidget {
   AnimatedListViewView({super.key});


  var items = [
    RadioItem("Car", "4 wheeker"),
    RadioItem("Bike", "2 Wheeler"),
    RadioItem("Car", "4 wheeker"),
    RadioItem("Bike", "2 Wheeler"),
    RadioItem("Car", "4 wheeker"),
    RadioItem("Bike", "2 Wheeler"),
    RadioItem("Car", "4 wheeker"),
    RadioItem("Bike", "2 Wheeler"),
    RadioItem("Car", "4 wheeker"),
    RadioItem("Bike", "2 Wheeler"),
    RadioItem("Car", "4 wheeker"),
    RadioItem("Bike", "2 Wheeler"),
    RadioItem("Car", "4 wheeker"),
    RadioItem("Bike", "2 Wheeler"),
    RadioItem("Car", "4 wheeker"),
    RadioItem("Bike", "2 Wheeler"),
    RadioItem("Car", "4 wheeker"),
    RadioItem("Bike", "2 Wheeler"),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: MasterAnimatedListView(
            items: items,
            once: true,
            animationVariant: AnimationVariant.slide,
            itemBuilder: (context, index) => GestureDetector(
              onTap: () {
                print(items[index]);
              },
              child: Container(
                width: double.infinity,
                height: 120.0,
                margin: const EdgeInsets.only(bottom: 16.0),
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.circular(16.0),
                ),
                child: Center(
                  child: Text(items[index].name),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

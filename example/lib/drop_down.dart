import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:master_ui_collection/core/widgets/dropdown/master_dropdown.dart';

import 'model/model_item.dart';

class DropDownList extends StatefulWidget {
  const DropDownList({Key? key}) : super(key: key);

  @override
  State<DropDownList> createState() => _DropDownListState();
}

class _DropDownListState extends State<DropDownList> {
  var value = modelList[0];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Padding(
        padding: const EdgeInsets.all(16.0),
        child: SizedBox(
          width: 200,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MasterDropdown<ModelItem>(
                displayText: (item) => item.name,
                value: value,
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(20)),
                borderRadius: 16,
                items: modelList,
                // icon: const Icon(CupertinoIcons.add_circled_solid),
                onChanged: (selectedItem) {
                  value = selectedItem!;
                  setState(() {
                  });
                },
              ),

              MasterDropdown<ModelItem>(
                displayText: (item) => item.name,
                value: value,
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(16)),
                items: modelList,
                icon: const Icon(CupertinoIcons.add_circled_solid),
                onChanged: (selectedItem) {
                  value = selectedItem!;
                  setState(() {
                  });
                },
              ),
              MasterDropdown<ModelItem>(
                displayText: (item) => item.name,
                value: value,
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(16),color: Colors.green),
                items: modelList,
                onChanged: (selectedItem) {
                  value = selectedItem!;
                  setState(() {
                  });
                },
              ),
            ],
          ),
        ),
      )
    );
  }
}

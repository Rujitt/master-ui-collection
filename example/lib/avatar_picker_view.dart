import 'package:flutter/material.dart';
import 'package:master_ui_collection/core/widgets/avatar_picker/master_avatar_picker.dart';
import 'package:master_ui_collection/masterui.dart';

class AvatarPickerView extends StatelessWidget {
  const AvatarPickerView({super.key});

  @override
  Widget build(BuildContext context) {
    return  SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const MasterAvatarPicker(),
                const SizedBox(height: 16.0),
                MasterAvatarPicker(
                  defaultImage: const NetworkImage('https://picsum.photos/200'),
                  radius: 50.0,
                  onImagePicked: (p0) => print(p0.path),
                  icon: const Icon(
                    Icons.add_a_photo_outlined,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

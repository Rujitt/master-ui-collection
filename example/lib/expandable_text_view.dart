import 'package:flutter/material.dart';
import 'package:master_ui_collection/core/widgets/expandable_text/master_expandable_text.dart';
import 'package:master_ui_collection/masterui.dart';

const String giantUnreadableText = '''
Amet risus nullam eget felis eget nunc lobortis. Donec et odio pellentesque diam volutpat. Aliquam id diam maecenas ultricies. Sapien et ligula ullamcorper malesuada proin libero nunc consequat. Quis vel eros donec ac odio tempor orci dapibus. Mi eget mauris pharetra et ultrices neque ornare aenean euismod. Lacinia at quis risus sed vulputate. Mattis molestie a iaculis at erat. Varius morbi enim nunc faucibus. Integer eget aliquet nibh praesent. Lobortis mattis aliquam faucibus purus. Integer feugiat scelerisque varius morbi enim nunc. Condimentum id venenatis a condimentum vitae sapien pellentesque habitant. Tempus imperdiet nulla malesuada pellentesque elit eget gravida. Hendrerit gravida rutrum quisque non tellus orci ac. Morbi tincidunt augue interdum velit. Praesent elementum facilisis leo vel fringilla est ullamcorper. Interdum velit euismod in pellentesque massa placerat duis ultricies lacus. Phasellus faucibus scelerisque eleifend donec pretium vulputate sapien.
''';

class ExpandableTextView extends StatelessWidget {
  const ExpandableTextView({super.key});

  @override
  Widget build(BuildContext context) {
    return const SafeArea(
      child: Scaffold(
        body: Padding(
          padding: EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                MasterExpandableText(
                  text: giantUnreadableText,
                  expandOnTextTap: false,
                  showLessText: "Show Less",
                  showMoreText: "Show More",
                  animationCurve: Curves.linear,
                ),
                SizedBox(height: spacing24,),
                MasterExpandableText(
                  text: giantUnreadableText,
                  textStyle: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.green,
                  ),
                  left: true,
                  showMore: Icon(Icons.arrow_circle_down),
                  showLess: Icon(Icons.arrow_circle_up),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

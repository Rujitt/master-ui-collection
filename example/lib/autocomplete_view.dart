import 'package:flutter/material.dart';
import 'package:master_ui_collection/masterui.dart';

class AutocompleteView<T> extends StatelessWidget {
  const AutocompleteView({super.key});

  @override
  Widget build(BuildContext context) {
    List<AutocompleteItem> items = List.generate(
      10,
      (index) => AutocompleteItem(
        title: 'Item $index',
        value: index.toString(),
        subtitle: 'Subtitle $index',
        leading: const Icon(Icons.list),
        trailing: const Icon(Icons.delete),
      ),
    );
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: MasterAutoComplete(
            items: items,
          ),
        ),
      ),
    );
  }
}

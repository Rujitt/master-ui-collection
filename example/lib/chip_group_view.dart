import 'package:flutter/material.dart';
import 'package:master_ui_collection/core/widgets/chips/master_chip_group.dart';

class ChipGroupView extends StatelessWidget {
  const ChipGroupView({super.key});

  @override
  Widget build(BuildContext context) {
    List<String> items = List.generate(25, (index) => 'Item ${index + 1}');
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                MasterChipGroup(
                  items: items,
                  labelBuilder: (context, item, _) => Text(item.toString()),
                  deleteIcon: const Icon(Icons.delete),
                  multiSelect: true,
                ),
                const Divider(height: 30),
                MasterChipGroup(
                  items: items,
                  labelBuilder: (context, item, _) => Text(
                    item.toString(),
                    style: const TextStyle(color: Colors.white),
                  ),
                  avatarBuilder: (context, item, index) => CircleAvatar(
                    child: Center(
                      child: Text(index.toString()),
                    ),
                  ),
                  backgroundColor: Colors.red,
                  selectedColor: Colors.green,
                  checkmarkColor: Colors.black,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:example/radio_view.dart';
import 'package:flutter/material.dart';
import 'package:master_ui_collection/masterui.dart';

class CheckBoxView extends StatefulWidget {
  const CheckBoxView({super.key});

  @override
  State<CheckBoxView> createState() => _CheckBoxViewState();
}

bool finalValue = false;

var checkList = [RadioItem("Car", "4 wheeker"), RadioItem("Bike", "2 Wheeler")];
var selectedItems = [];

class _CheckBoxViewState extends State<CheckBoxView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          ...checkList
              .map((e) => MasterCheckbox(
                    onChanged: (value) {
                      if (value) {
                        selectedItems.add(e);
                      } else {
                        selectedItems.remove(e);
                      }
                      setState(() {});
                    },
                    title: e.name,
                    isTrailing: true,
                    isLeading: false,
                    borderColor: Colors.deepPurpleAccent,
                    width: 40,
                    borderWidth: 2,
                    selectedTileColor: Colors.redAccent,
                    checkColor: Colors.redAccent,
                  ))
              .toList(),
          ...checkList
              .map((e) => MasterCheckbox(
                    onChanged: (value) {
                      if (value) {
                        selectedItems.add(e);
                      } else {
                        selectedItems.remove(e);
                      }
                      setState(() {});
                    },
                    title: e.name,
                    isTrailing: true,
                    isLeading: false,
                    checkColor: Colors.redAccent,
                  ))
              .toList(),
          ...checkList
              .map((e) => MasterCheckbox(
                    onChanged: (value) {
                      print(value);
                      if (value) {
                        selectedItems.add(e);
                      } else {
                        selectedItems.remove(e);
                      }
                      setState(() {});
                    },
                    title: e.name,
                    isTrailing: true,
                    isLeading: false,
                    borderRadius: 20,
                    checkColor: Colors.redAccent,
                  ))
              .toList(),
          ...checkList
              .map((e) => MasterCheckbox(
                    onChanged: (value) {
                      print(value);
                      if (value) {
                        selectedItems.add(e);
                      } else {
                        selectedItems.remove(e);
                      }
                      setState(() {});
                    },
                    title: e.name,
                    isTrailing: false,
                    isLeading: false,
                    borderRadius: 20,
                    checkColor: Colors.redAccent,
                  ))
              .toList(),
          ...checkList
              .map((e) => MasterCheckbox(
                    onChanged: (value) {
                      if (value) {
                        selectedItems.add(e);
                      } else {
                        selectedItems.remove(e);
                      }
                      setState(() {});
                    },
                    title: e.name,
                    isCustom: true,
                    checkWidget: const Icon(Icons.check_box),
                    unCheckWidget: const Icon(Icons.check_box_outline_blank),
                    borderRadius: 20,
                    checkColor: Colors.redAccent,
                  ))
              .toList(),
          ...checkList
              .map((e) => MasterCheckbox(
                    onChanged: (value) {
                      if (value) {
                        selectedItems.add(e);
                      } else {
                        selectedItems.remove(e);
                      }
                      setState(() {});
                    },
                    title: e.name,
                    checkWidget: const Icon(Icons.ac_unit_sharp),
                    borderRadius: 20,
                    selectedTileColor: Colors.lightGreen,
                    checkColor: Colors.redAccent,
                  ))
              .toList()
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:master_ui_collection/masterui.dart';

class PaginationView extends StatelessWidget {
  const PaginationView({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: MasterPagination(
            total: 10,
            value: 1,
            onChanged: (value) => debugPrint("NEW PAGE IS $value"),
            siblings: 2,
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:master_ui_collection/core/widgets/onboarding/master_onboarding.dart';

class OnboardingView extends StatelessWidget {
  const OnboardingView({super.key});

  @override
  Widget build(BuildContext context) {
    List<String> items = [
      "HELLO",
      "IT'S",
      "DOCTOR",
      "MICAHEL",
      "MORBIUS",
      "AT",
      "YOUR",
      "SERVICE",
    ];
    return SafeArea(
      child: Scaffold(
        body: MasterOnboarding(
          items: items,
          itemBuilder: (context, index) => Center(
            child: Text(
              items[index],
              style: Theme.of(context).textTheme.headlineLarge,
            ),
          ),
        ),
      ),
    );
  }
}

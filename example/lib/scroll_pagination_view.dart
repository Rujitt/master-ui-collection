// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';

import 'package:master_ui_collection/masterui.dart';

class ScrollPaginationView extends StatefulWidget {
  const ScrollPaginationView({super.key});

  @override
  State<ScrollPaginationView> createState() => _ScrollPaginationViewState();
}

class _ScrollPaginationViewState extends State<ScrollPaginationView> {
  String? _next;
  List<People> _people = [];
  bool isLoading = true;

  Future<void> _fetchPeople([int page = 1]) async {
    try {
      setState(() => isLoading = true);
      Uri url = Uri.https(
        "swapi.dev",
        "/api/people",
        {"page": page.toString()},
      );
      Response response = await get(url);
      PaginatedPeople paginatedPeople = PaginatedPeople.fromJson(response.body);
      setState(() {
        isLoading = false;
        _next = paginatedPeople.next;
        _people = [..._people, ...paginatedPeople.results];
      });
    } finally {
      setState(() => isLoading = false);
    }
  }

  void handleLoadMore() {
    // get page from next url
    if (_next == null) return;
    int page = int.parse(_next!.split("=").last);
    _fetchPeople(page);
  }

  @override
  void initState() {
    super.initState();
    _fetchPeople();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: _people.isEmpty
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : Padding(
                padding: const EdgeInsets.all(16.0),
                child: MasterScrollPagination(
                  items: _people,
                  itemBuilder: (person) {
                    return ListTile(
                      title: Text(person.name),
                      subtitle: Text(person.gender),
                    );
                  },
                  onLoadMore: handleLoadMore,
                  isLoading: isLoading,
                ),
              ),
      ),
    );
  }
}

class People {
  final String name;
  final String gender;

  const People({
    required this.name,
    required this.gender,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'name': name,
      'gender': gender,
    };
  }

  factory People.fromMap(Map<String, dynamic> map) {
    return People(
      name: map['name'] as String,
      gender: map['gender'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory People.fromJson(String source) =>
      People.fromMap(json.decode(source) as Map<String, dynamic>);
}

class PaginatedPeople {
  final String? next;
  final String? previous;
  final List<People> results;

  const PaginatedPeople({
    required this.next,
    required this.previous,
    required this.results,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'next': next,
      'previous': previous,
      'results': results.map((x) => x.toMap()).toList(),
    };
  }

  factory PaginatedPeople.fromMap(Map<String, dynamic> map) {
    return PaginatedPeople(
      next: map['next'] != null ? map['next'] as String : null,
      previous: map['previous'] != null ? map['previous'] as String : null,
      results: List<People>.from(
        (map['results'] as List<dynamic>).map<People>(
          (x) => People.fromMap(x as Map<String, dynamic>),
        ),
      ),
    );
  }

  String toJson() => json.encode(toMap());

  factory PaginatedPeople.fromJson(String source) =>
      PaginatedPeople.fromMap(json.decode(source) as Map<String, dynamic>);
}

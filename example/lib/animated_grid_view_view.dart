import 'package:example/radio_view.dart';
import 'package:flutter/material.dart';
import 'package:master_ui_collection/masterui.dart';

class AnimatedGridViewView extends StatelessWidget {
   AnimatedGridViewView({super.key});
  var items = [
    RadioItem("Car", "4 wheeker"),
    RadioItem("Bike", "2 Wheeler"),
    RadioItem("Car", "4 wheeker"),
    RadioItem("Bike", "2 Wheeler"),
    RadioItem("Car", "4 wheeker"),
    RadioItem("Bike", "2 Wheeler"),
    RadioItem("Car", "4 wheeker"),
    RadioItem("Bike", "2 Wheeler"),
    RadioItem("Car", "4 wheeker"),
    RadioItem("Bike", "2 Wheeler"),
    RadioItem("Car", "4 wheeker"),
    RadioItem("Bike", "2 Wheeler"),
    RadioItem("Car", "4 wheeker"),
    RadioItem("Bike", "2 Wheeler"),
    RadioItem("Car", "4 wheeker"),
    RadioItem("Bike", "2 Wheeler"),
    RadioItem("Car", "4 wheeker"),
    RadioItem("Bike", "2 Wheeler"),
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: MasterAnimatedGridView(
            items: items,
            crossAxisCount: 4,
            mainAxisSpacing: 16.0,
            crossAxisSpacing: 16.0,
            animationVariant: AnimationVariant.slide,
            duration: const Duration(milliseconds: 300),
            once: true,
            itemBuilder: (context, index) {
              return Container(
                width: double.infinity,
                height: 120.0,
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.primary,
                  borderRadius: const BorderRadius.all(borderRadius8)
                ),
                child: Center(
                  child: Text(
                    items[index].cast,
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

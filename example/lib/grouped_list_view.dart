import 'package:flutter/material.dart';
import 'package:master_ui_collection/core/widgets/grouped_list/master_grouped_list.dart';
import 'package:master_ui_collection/masterui.dart';

const String loremText = '''
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
''';

class GroupedListView extends StatelessWidget {
  const GroupedListView({super.key});

  @override
  Widget build(BuildContext context) {
    List<GroupedListItem<String>> items = List.generate(
      10,
      (index) => GroupedListItem(
        title: 'Group $index',
        item: loremText,
      ),
    );
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: MasterGroupedList(
            items: items,
            groupItemBuilder: (context, item) {
              return ListTile(
                title: Text(item),
              );
            },
            groupHeaderBuilder: (context, title) {
              return SizedBox(
                width: double.infinity,
                child: Container(
                  color: Colors.red,
                  padding: const EdgeInsets.all(16.0),
                  child: Text(title),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

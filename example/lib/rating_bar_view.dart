import 'package:flutter/material.dart';
import 'package:master_ui_collection/masterui.dart';

class RatingBarView extends StatelessWidget {
  const RatingBarView({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MasterRatingBar(
                maxRating: 10,
                rating: 8.5,
                onRatingChanged: (rating) => debugPrint(
                  rating.toString(),
                ),
              ),
              const SizedBox(height: 16.0),
              MasterRatingBar(
                rating: 2.5,
                onRatingChanged: (rating) => debugPrint(
                  rating.toString(),
                ),
              ),
              const SizedBox(height: 16.0),
              const MasterRatingBar(
                rating: 4.5,
                filledIcon: Icons.star_rounded,
                halfFilledIcon: Icons.star_half_rounded,
                emptyIcon: Icons.star_border_rounded,
              ),
              const SizedBox(height: 16.0),
              const MasterRatingBar(
                rating: 2.5,
                filledIcon: Icons.star_rounded,
                halfFilledIcon: Icons.star_half_rounded,
                emptyIcon: Icons.star_border_rounded,
                filledColor: Colors.green,
                emptyColor: Colors.red,
              ),
              const SizedBox(height: 16.0),
              const MasterRatingBar(
                rating: 2.5,
                filledIcon: Icons.heart_broken_outlined,
                halfFilledIcon: Icons.heart_broken_rounded,
                emptyIcon: Icons.heart_broken_sharp,
                filledColor: Colors.green,
                emptyColor: Colors.red,
                size: 50,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

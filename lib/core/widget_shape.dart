
import 'package:flutter/cupertino.dart';
enum SuperShapeBorder {
  circle,
  oval,
  beveledRectangle,
  stadium,
}

ShapeBorder getShapeBorder(SuperShapeBorder shape) {
  switch (shape) {
    case SuperShapeBorder.circle:
      return const CircleBorder();
    case SuperShapeBorder.oval:
      return RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      );
    case SuperShapeBorder.beveledRectangle:
      return BeveledRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      );
    case SuperShapeBorder.stadium:
      return const StadiumBorder();
    default:
      return const CircleBorder();
  }
}
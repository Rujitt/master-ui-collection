import 'package:flutter/cupertino.dart';

const Radius borderRadius0 = Radius.zero;
const Radius borderRadius2 = Radius.circular(2);
const Radius borderRadius4 = Radius.circular(4);
const Radius borderRadius8 = Radius.circular(8);
const Radius borderRadius16 = Radius.circular(16);

import 'package:flutter/material.dart';


class MasterTabBar2 extends StatefulWidget {
  final int itemCount;
  final IndexedWidgetBuilder tabBuilder;
  final IndexedWidgetBuilder pageBuilder;
  final Widget? stub;
  final ValueChanged<int> onPositionChange;
  final ValueChanged<double> onScroll;
  final int initPosition;
  final double indicatorHeight;
  final Color? indicatorColor;
  final double borderRadius;
  final Color? unselectedLabelColor;
  final Decoration? customIndicator;
  final bool? scroolable;

  const MasterTabBar2({super.key,
    required this.itemCount,
    required this.tabBuilder,
    required this.pageBuilder,
    this.stub,
    required this.onPositionChange,
    required this.onScroll,
    required this.initPosition,
    this.indicatorHeight = 0.0,
    this.indicatorColor,
    this.borderRadius = 2.0,
    this.unselectedLabelColor,
    this.customIndicator,
    this.scroolable = false,
  });

  @override
  _CustomTabsState createState() => _CustomTabsState();
}

class _CustomTabsState extends State<MasterTabBar2>
    with TickerProviderStateMixin {
  late TabController controller;
  int? _currentCount;
  int? _currentPosition;

  @override
  void initState() {
    _currentPosition = widget.initPosition;
    controller = TabController(
      length: widget.itemCount,
      vsync: this,
      initialIndex: _currentPosition!,
    );
    controller.addListener(onPositionChange);
    controller.animation?.addListener(onScroll);
    _currentCount = widget.itemCount;
    super.initState();
  }

  @override
  void didUpdateWidget(MasterTabBar2 oldWidget) {
    if (_currentCount != widget.itemCount) {
      controller.animation?.removeListener(onScroll);
      controller.removeListener(onPositionChange);
      controller.dispose();

      _currentPosition = widget.initPosition;

      if (_currentPosition! > widget.itemCount - 1) {
        _currentPosition = widget.itemCount - 1;
        _currentPosition = _currentPosition! < 0 ? 0 : _currentPosition;
        WidgetsBinding.instance.addPostFrameCallback((_) {
          if (mounted) {
            widget.onPositionChange(_currentPosition!);
          }
        });
      }

      _currentCount = widget.itemCount;
      setState(() {
        controller = TabController(
          length: widget.itemCount,
          vsync: this,
          initialIndex: _currentPosition!,
        );
        controller.addListener(onPositionChange);
        controller.animation?.addListener(onScroll);
      });
    } else {
      controller.animateTo(widget.initPosition);
    }


    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    controller.animation?.removeListener(onScroll);
    controller.removeListener(onPositionChange);
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.itemCount < 1) return widget.stub ?? Container();

    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            height: widget.indicatorHeight,

            alignment: Alignment.topLeft,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: TabBar(
                labelPadding: const EdgeInsets.symmetric(horizontal: 16),
                isScrollable: widget.scroolable!,
                controller: controller,
                indicatorColor: widget.indicatorColor ?? Colors.blueAccent,
                labelColor: widget.indicatorColor,
                unselectedLabelColor:
                widget.unselectedLabelColor ?? Colors.amberAccent,
                indicator: widget.customIndicator,
                tabs: List.generate(
                  widget.itemCount,
                      (index) => widget.tabBuilder(context, index),
                ),
              ),
            ),
          ),
          // Expanded(
          //   child: TabBarView(
          //     controller: controller,
          //     physics: NeverScrollableScrollPhysics(),
          //     children: List.generate(
          //       widget.itemCount,
          //           (index) => widget.pageBuilder(context, index),
          //     ),
          //   ),
          // ),
        ],

    );
  }

  onPositionChange() {
    if (!controller.indexIsChanging) {
      _currentPosition = controller.index;
      widget.onPositionChange(_currentPosition!);
    }
  }

  onScroll() {
    widget.onScroll(controller.animation!.value);
  }
}
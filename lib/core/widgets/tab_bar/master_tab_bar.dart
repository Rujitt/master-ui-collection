import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MasterTabBar<T> extends StatelessWidget {
  final List<Widget> tabs;
  final List<T> items;
  final Widget Function(BuildContext context, int index, T item)
      tabBarViewItembuilder;
  final int? initialIndex;
  final Duration? animationDuration;
  final bool renderWithoutAppBar;
  final AppBarProps? appBarProps;
  final TabBarViewProps? tabBarViewProps;
  final TabBarProps? tabBarProps;
  final ScaffoldProps? scaffoldProps;

  const MasterTabBar({
    super.key,
    required this.tabs,
    required this.items,
    required this.tabBarViewItembuilder,
    this.initialIndex,
    this.animationDuration,
    this.renderWithoutAppBar = false,
    this.appBarProps,
    this.tabBarViewProps,
    this.tabBarProps,
    this.scaffoldProps,
  })  : assert(
          tabs.length == items.length,
          "tabs and tabBarViews must be of same length",
        ),
        assert(
          renderWithoutAppBar ? appBarProps == null : true,
          "appBarProps can only be used when renderWithoutAppBar is false",
        );

  @override
  Widget build(BuildContext context) {
    return _buildTabController(context);
  }

  DefaultTabController _buildTabController(BuildContext context) {
    return DefaultTabController(
      initialIndex: initialIndex ?? 0,
      length: tabs.length,
      animationDuration: animationDuration,
      child: Scaffold(
        drawer: scaffoldProps?.drawer,
        backgroundColor: scaffoldProps?.backgroundColor,
        floatingActionButton: scaffoldProps?.floatingActionButton,
        floatingActionButtonLocation:
            scaffoldProps?.floatingActionButtonLocation,
        bottomNavigationBar: scaffoldProps?.bottomNavigationBar,
        restorationId: scaffoldProps?.restorationId,
        persistentFooterButtons: scaffoldProps?.persistentFooterButtons,
        appBar: scaffoldProps?.appBar ??
            AppBar(
              key: appBarProps?.key,
              leading: appBarProps?.leading,
              automaticallyImplyLeading:
                  appBarProps?.automaticallyImplyLeading ?? true,
              title: appBarProps?.title,
              actions: appBarProps?.actions,
              elevation: appBarProps?.elevation,
              scrolledUnderElevation: appBarProps?.scrolledUnderElevation,
              notificationPredicate: appBarProps?.notificationPredicate ??
                  defaultScrollNotificationPredicate,
              shadowColor: appBarProps?.shadowColor,
              surfaceTintColor: appBarProps?.surfaceTintColor,
              shape: appBarProps?.shape,
              backgroundColor: appBarProps?.backgroundColor,
              foregroundColor: appBarProps?.foregroundColor,
              iconTheme: appBarProps?.iconTheme,
              actionsIconTheme: appBarProps?.actionsIconTheme,
              primary: appBarProps?.primary ?? true,
              centerTitle: appBarProps?.centerTitle,
              excludeHeaderSemantics:
                  appBarProps?.excludeHeaderSemantics ?? false,
              titleSpacing: appBarProps?.titleSpacing,
              toolbarOpacity: appBarProps?.toolbarOpacity ?? 1.0,
              bottomOpacity: appBarProps?.bottomOpacity ?? 1.0,
              toolbarHeight: appBarProps?.toolbarHeight,
              leadingWidth: appBarProps?.leadingWidth,
              toolbarTextStyle: appBarProps?.toolbarTextStyle,
              titleTextStyle: appBarProps?.titleTextStyle,
              systemOverlayStyle: appBarProps?.systemOverlayStyle,
              forceMaterialTransparency:
                  appBarProps?.forceMaterialTransparency ?? false,
              clipBehavior: appBarProps?.clipBehavior,
              flexibleSpace: appBarProps?.flexibleSpace ??
                  (renderWithoutAppBar
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            _buildTabBar(),
                          ],
                        )
                      : null),
              bottom: appBarProps?.bottom ??
                  (renderWithoutAppBar ? null : _buildTabBar()),
            ),
        body: scaffoldProps?.body ??
            TabBarView(
              key: tabBarViewProps?.key,
              clipBehavior: tabBarViewProps?.clipBehavior ?? Clip.hardEdge,
              controller: tabBarViewProps?.controller,
              physics: tabBarViewProps?.physics,
              dragStartBehavior:
                  tabBarViewProps?.dragStartBehavior ?? DragStartBehavior.start,
              viewportFraction: tabBarViewProps?.viewportFraction ?? 1.0,
              children: tabBarViewProps?.children ??
                  items
                      .map(
                        (tabBarView) => tabBarViewItembuilder(
                          context,
                          items.indexOf(tabBarView),
                          tabBarView,
                        ),
                      )
                      .toList(),
            ),
      ),
    );
  }

  TabBar _buildTabBar() {
    return TabBar(
      key: tabBarProps?.key,
      tabs: tabBarProps?.tabs ?? tabs,
      indicatorColor: tabBarProps?.indicatorColor,
      indicatorWeight: tabBarProps?.indicatorWeight ?? 2.0,
      indicatorPadding: tabBarProps?.indicatorPadding ?? EdgeInsets.zero,
      indicator: tabBarProps?.indicator,
      indicatorSize: tabBarProps?.indicatorSize ?? TabBarIndicatorSize.tab,
      labelColor: tabBarProps?.labelColor,
      labelStyle: tabBarProps?.labelStyle,
      labelPadding: tabBarProps?.labelPadding ?? EdgeInsets.zero,
      unselectedLabelColor: tabBarProps?.unselectedLabelColor,
      unselectedLabelStyle: tabBarProps?.unselectedLabelStyle,
      dragStartBehavior:
          tabBarProps?.dragStartBehavior ?? DragStartBehavior.start,
      isScrollable: tabBarProps?.isScrollable ?? false,
      padding: tabBarProps?.padding,
      controller: tabBarProps?.controller,
      onTap: tabBarProps?.onTap,
    );
  }
}

class AppBarProps {
  final Key? key;
  final Widget? leading;
  final bool automaticallyImplyLeading;
  final Widget? title;
  final List<Widget>? actions;
  final Widget? flexibleSpace;
  final PreferredSizeWidget? bottom;
  final double? elevation;
  final double? scrolledUnderElevation;
  final bool Function(ScrollNotification) notificationPredicate;
  final Color? shadowColor;
  final Color? surfaceTintColor;
  final ShapeBorder? shape;
  final Color? backgroundColor;
  final Color? foregroundColor;
  final IconThemeData? iconTheme;
  final IconThemeData? actionsIconTheme;
  final bool primary;
  final bool? centerTitle;
  final bool excludeHeaderSemantics;
  final double? titleSpacing;
  final double toolbarOpacity;
  final double bottomOpacity;
  final double? toolbarHeight;
  final double? leadingWidth;
  final TextStyle? toolbarTextStyle;
  final TextStyle? titleTextStyle;
  final SystemUiOverlayStyle? systemOverlayStyle;
  final bool forceMaterialTransparency;
  final Clip? clipBehavior;

  AppBarProps({
    this.key,
    this.leading,
    this.automaticallyImplyLeading = true,
    this.title,
    this.actions,
    this.flexibleSpace,
    this.bottom,
    this.elevation,
    this.scrolledUnderElevation,
    this.notificationPredicate = defaultScrollNotificationPredicate,
    this.shadowColor,
    this.surfaceTintColor,
    this.shape,
    this.backgroundColor,
    this.foregroundColor,
    this.iconTheme,
    this.actionsIconTheme,
    this.primary = true,
    this.centerTitle,
    this.excludeHeaderSemantics = false,
    this.titleSpacing,
    this.toolbarOpacity = 1.0,
    this.bottomOpacity = 1.0,
    this.toolbarHeight,
    this.leadingWidth,
    this.toolbarTextStyle,
    this.titleTextStyle,
    this.systemOverlayStyle,
    this.forceMaterialTransparency = false,
    this.clipBehavior,
  });
}

class TabBarViewProps {
  final Key? key;
  final List<Widget> children;
  final TabController? controller;
  final ScrollPhysics? physics;
  final DragStartBehavior dragStartBehavior;
  final double viewportFraction;
  final Clip clipBehavior;

  TabBarViewProps({
    this.key,
    required this.children,
    this.controller,
    this.physics,
    this.dragStartBehavior = DragStartBehavior.start,
    this.viewportFraction = 1.0,
    this.clipBehavior = Clip.hardEdge,
  });
}

class TabBarProps {
  final Key? key;
  final List<Widget>? tabs;
  final Color? indicatorColor;
  final double? indicatorWeight;
  final EdgeInsetsGeometry? indicatorPadding;
  final Decoration? indicator;
  final TabBarIndicatorSize? indicatorSize;
  final Color? labelColor;
  final TextStyle? labelStyle;
  final EdgeInsetsGeometry? labelPadding;
  final Color? unselectedLabelColor;
  final TextStyle? unselectedLabelStyle;
  final DragStartBehavior? dragStartBehavior;
  final bool? isScrollable;
  final EdgeInsetsGeometry? padding;
  final Color? backgroundColor;
  final double? tabBarHeight;
  final double? tabBarWidth;
  final TabController? controller;
  final ValueChanged<int>? onTap;

  TabBarProps({
    this.key,
    this.tabs,
    this.indicatorColor,
    this.indicatorWeight,
    this.indicatorPadding,
    this.indicator,
    this.indicatorSize,
    this.labelColor,
    this.labelStyle,
    this.labelPadding,
    this.unselectedLabelColor,
    this.unselectedLabelStyle,
    this.dragStartBehavior,
    this.isScrollable,
    this.padding,
    this.backgroundColor,
    this.tabBarHeight,
    this.tabBarWidth,
    this.controller,
    this.onTap,
  });
}

class ScaffoldProps {
  final PreferredSizeWidget? appBar;
  final Widget? body;
  final Widget? floatingActionButton;
  final FloatingActionButtonLocation? floatingActionButtonLocation;
  final Widget? drawer;
  final Widget? bottomNavigationBar;
  final List<Widget>? persistentFooterButtons;
  final Color? backgroundColor;
  final String? restorationId;

  ScaffoldProps({
    this.appBar,
    this.body,
    this.floatingActionButton,
    this.floatingActionButtonLocation,
    this.drawer,
    this.bottomNavigationBar,
    this.persistentFooterButtons,
    this.backgroundColor,
    this.restorationId,
  });
}

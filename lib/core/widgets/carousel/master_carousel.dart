import 'dart:async';

import 'package:flutter/material.dart';

class MasterCarousel extends StatefulWidget {
  final List<Widget> children;
  final double height;
  final int initialPage;
  final bool autoPlay;
  final Duration pageChangeDuration;
  final Curve pageChangeCurve;
  final double pageSpacing;
  final bool pauseAutoPlayOnTouch;
  final Duration autoPlayInterval;
  final bool showControls;
  final Widget leftControl;
  final Widget rightControl;
  final double controlsSpacing;
  final CarouselIndicatorProps? indicatorProps;

  const MasterCarousel({
    super.key,
    required this.children,
    required this.height,
    this.initialPage = 0,
    this.autoPlay = false,
    this.pageChangeDuration = const Duration(milliseconds: 300),
    this.pageChangeCurve = Curves.easeIn,
    this.pageSpacing = 4.0,
    this.autoPlayInterval = const Duration(seconds: 1),
    this.pauseAutoPlayOnTouch = false,
    this.showControls = false,
    this.leftControl = const Icon(Icons.arrow_back_ios),
    this.rightControl = const Icon(Icons.arrow_forward_ios),
    this.controlsSpacing = 16.0,
    this.indicatorProps,
  }) : assert(
          initialPage >= 0 && initialPage < children.length,
          "initialPage must be between 0 and ${children.length - 1}",
        );

  @override
  State<MasterCarousel> createState() => _MasterCarouselState();
}

class _MasterCarouselState extends State<MasterCarousel> {
  late PageController _controller;
  late int _currentPage;
  late Timer? _timer;

  void _animateToNextPage() {
    final nextPage =
        _currentPage < widget.children.length - 1 ? _currentPage + 1 : 0;

    if (_controller.hasClients) {
      _controller.animateToPage(
        nextPage,
        duration: widget.pageChangeDuration,
        curve: widget.pageChangeCurve,
      );
    }
  }

  void _animateToPreviousPage() {
    final previousPage =
        _currentPage > 0 ? _currentPage - 1 : widget.children.length - 1;

    if (_controller.hasClients) {
      _controller.animateToPage(
        previousPage,
        duration: widget.pageChangeDuration,
        curve: widget.pageChangeCurve,
      );
    }
  }

  Timer _autoPlay() {
    return Timer.periodic(
      widget.autoPlayInterval,
      (_) {
        _animateToNextPage();
      },
    );
  }

  @override
  void initState() {
    super.initState();
    _controller = PageController(initialPage: widget.initialPage);
    _currentPage = widget.initialPage;
    if (widget.autoPlay) {
      _timer = _autoPlay();
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          children: [
            if (widget.showControls)
              Row(
                children: [
                  GestureDetector(
                    onTap: _animateToPreviousPage,
                    child: widget.leftControl,
                  ),
                  SizedBox(width: widget.controlsSpacing),
                ],
              ),
            Expanded(
              child: SizedBox(
                height: widget.height,
                child: PageView.builder(
                  itemBuilder: (context, index) {
                    Widget child = widget.children[index];
                    bool isFirstChild = index == 0;
                    bool isLastChild = index == widget.children.length - 1;
                    return GestureDetector(
                      onPanUpdate: (details) {
                        // Swiping in right direction.
                        if (details.delta.dx > 0) {
                          _animateToPreviousPage();
                        }

                        // Swiping in left direction.
                        if (details.delta.dx < 0) {
                          _animateToNextPage();
                        }
                      },
                      onLongPressStart: (_) {
                        if (widget.pauseAutoPlayOnTouch) _timer?.cancel();
                      },
                      onLongPressEnd: (_) {
                        if (widget.pauseAutoPlayOnTouch && widget.autoPlay) {
                          _timer = _autoPlay();
                        }
                      },
                      child: Padding(
                        padding: EdgeInsets.only(
                          left: isFirstChild ? 0 : widget.pageSpacing,
                          right: isLastChild ? 0 : widget.pageSpacing,
                        ),
                        child: child,
                      ),
                    );
                  },
                  itemCount: widget.children.length,
                  controller: _controller,
                  onPageChanged: (index) {
                    setState(() {
                      _currentPage = index;
                    });
                  },
                ),
              ),
            ),
            if (widget.showControls)
              Row(
                children: [
                  SizedBox(width: widget.controlsSpacing),
                  GestureDetector(
                    onTap: _animateToNextPage,
                    child: widget.rightControl,
                  ),
                ],
              )
          ],
        ),
        if (widget.indicatorProps != null)
          Column(
            children: [
              const SizedBox(height: 12.0),
              CarouselIndicators(
                length: widget.children.length,
                currentPage: _currentPage,
                indicatorProps: widget.indicatorProps,
                onIndicatorTap: (index) => _controller.animateToPage(
                  index,
                  duration: widget.pageChangeDuration,
                  curve: widget.pageChangeCurve,
                ),
              ),
            ],
          ),
        const SizedBox.shrink()
      ],
    );
  }
}

class CarouselIndicators extends StatelessWidget {
  final int length;
  final int currentPage;

  final CarouselIndicatorProps? indicatorProps;
  final Function(int index)? onIndicatorTap;

  const CarouselIndicators({
    super.key,
    required this.length,
    required this.currentPage,
    this.indicatorProps,
    this.onIndicatorTap,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: indicatorProps!.indicatorHeight + 10,
      child: Row(
        mainAxisAlignment:
            indicatorProps?.mainAxisAlignment ?? MainAxisAlignment.center,
        children: List.generate(
          length,
          (index) => GestureDetector(
            onTap: () => onIndicatorTap?.call(index),
            child: AnimatedContainer(
              duration: const Duration(milliseconds: 300),
              margin: EdgeInsets.only(
                left: index == 0 ? 0 : 4.0,
                right: index == length - 1 ? 0 : 4.0,
              ),
              width: index == currentPage
                  ? indicatorProps?.selectedIndicatorWidth
                  : indicatorProps?.indicatorWidth,
              height: index == currentPage
                  ? indicatorProps?.selectedIndicatorHeight
                  : indicatorProps?.indicatorHeight,
              decoration: BoxDecoration(
                color: index == currentPage
                    ? indicatorProps?.activeColor ??
                        Theme.of(context).primaryColor
                    : (indicatorProps?.inactiveColor ?? Colors.transparent),
                borderRadius:
                    BorderRadius.circular(indicatorProps!.indicatorRadius),
                border: Border.all(
                  color: indicatorProps!.borderColor,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class CarouselIndicatorProps {
  final Color borderColor;
  final Color activeColor;
  final Color inactiveColor;
  final double activeWidth;
  final MainAxisAlignment mainAxisAlignment;
  final double indicatorRadius;
  final double indicatorHeight;
  final double indicatorWidth;
  final double selectedIndicatorWidth;
  final double selectedIndicatorHeight;

  const CarouselIndicatorProps({
    this.borderColor = Colors.grey,
    this.activeColor = Colors.blue,
    this.inactiveColor = Colors.transparent,
    this.activeWidth = 32.0,
    this.mainAxisAlignment = MainAxisAlignment.center,
    this.indicatorRadius = 10,
    this.indicatorHeight = 8.0,
    this.selectedIndicatorWidth = 10,
    this.selectedIndicatorHeight = 10.0,
    this.indicatorWidth = 8.0,
  });
}

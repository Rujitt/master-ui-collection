// ignore_for_file: unused_element

import 'package:flutter/material.dart';

class MasterSnackbar {
  static show({
    required BuildContext externalContext,
    required Widget content,
    Color? backgroundColor,
    double? elevation,
    Duration duration = const Duration(seconds: 2),
    EdgeInsetsGeometry? padding,
  }) {
    ScaffoldMessenger.of(externalContext).showSnackBar(
      SnackBar(
        backgroundColor: backgroundColor,
        elevation: elevation,
        padding: padding,
        content: content,
        duration: duration,
      ),
    );
  }
}

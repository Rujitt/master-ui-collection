import 'package:flutter/material.dart';

const String dots = "...";

class MasterPagination extends StatefulWidget {
  final int total;
  final int siblings;
  final int value;
  final ValueChanged<int> onChanged;
  final double width;
  final double height;
  final double spacing;
  final Color? activeBackgroundColor;
  final Color? activeTextColor;
  final Color? inactiveBackgroundColor;
  final Color? inactiveTextColor;
  const MasterPagination({
    super.key,
    required this.total,
    required this.value,
    required this.onChanged,
    this.siblings = 1,
    this.width = 40,
    this.height = 40,
    this.spacing = 8,
    this.activeBackgroundColor,
    this.activeTextColor,
    this.inactiveBackgroundColor,
    this.inactiveTextColor,
  }) : assert(
          value > 0 && value <= total,
          "value must be between 1 and $total",
        );

  @override
  State<MasterPagination> createState() => _MasterPaginationState();
}

class _MasterPaginationState extends State<MasterPagination> {
  late int _value;

  void handlePaginationItemTap(String item) {
    if (item == dots) return;
    int value = int.parse(item);
    setState(() => _value = value);
    widget.onChanged(value);
  }

  @override
  void initState() {
    super.initState();
    _value = widget.value;
  }

  @override
  Widget build(BuildContext context) {
    List<String> items = _createPaginationItems();
    return Wrap(
      children: [
        ...items.map(
          (item) {
            bool isActive = _value.toString() == item;
            return Container(
              margin: EdgeInsets.only(
                right: widget.spacing,
                bottom: widget.spacing,
              ),
              child: InkWell(
                borderRadius: BorderRadius.circular(widget.height / 2),
                onTap:
                    item == dots ? null : () => handlePaginationItemTap(item),
                child: Ink(
                  width: widget.width,
                  height: widget.height,
                  decoration: BoxDecoration(
                    color: isActive
                        ? widget.activeBackgroundColor ??
                            Theme.of(context).primaryColor
                        : widget.inactiveBackgroundColor ?? Colors.grey[200],
                    borderRadius: BorderRadius.circular(
                      widget.height / 2,
                    ),
                  ),
                  child: Center(
                    child: item == dots
                        ? const Icon(Icons.more_horiz)
                        : Text(
                            item,
                            style: TextStyle(
                              color: isActive
                                  ? widget.activeTextColor ?? Colors.white
                                  : widget.inactiveTextColor,
                            ),
                          ),
                  ),
                ),
              ),
            );
          },
        ),
      ],
    );
  }

  List<String> _createPaginationItems() {
    List<int> items = List.generate(widget.total, (index) => index + 1);
    List<String> paginationItems = [];
    for (int i = 0; i < items.length; i++) {
      int item = items[i];
      bool isFirst = i == 0;
      bool isLast = i == items.length - 1;
      bool showPrev = item < _value && _value - item <= widget.siblings;
      bool showNext = item > _value && item - _value <= widget.siblings;
      if (isFirst || isLast || showPrev || showNext || item == _value) {
        paginationItems.add(item.toString());
      } else {
        paginationItems.add(dots);
      }
    }
    int activeIndex = paginationItems.indexOf(_value.toString());
    List<String> prevItems =
        paginationItems.sublist(0, activeIndex).toSet().toList();
    List<String> nextItems =
        paginationItems.sublist(activeIndex + 1).toSet().toList();
    return [...prevItems, _value.toString(), ...nextItems];
  }
}

import 'package:flutter/material.dart';

class MasterScrollPagination<T> extends StatefulWidget {
  final List<T> items;
  final Widget Function(T item) itemBuilder;
  final void Function() onLoadMore;
  final bool hasMore;
  final bool isLoading;
  final Widget? loadingWidget;
  const MasterScrollPagination({
    super.key,
    required this.items,
    required this.itemBuilder,
    required this.onLoadMore,
    this.hasMore = false,
    this.isLoading = false,
    this.loadingWidget,
  });

  @override
  State<MasterScrollPagination<T>> createState() =>
      _MasterScrollPaginationState<T>();
}

class _MasterScrollPaginationState<T> extends State<MasterScrollPagination<T>> {
  late ScrollController _scrollController;

  void _scrollListener() {
    // detect if the user has scrolled to the bottom of the list
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      widget.onLoadMore();
    }
  }

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController()..addListener(_scrollListener);
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.removeListener(_scrollListener);
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: ListView.builder(
            controller: _scrollController,
            itemCount: widget.items.length,
            itemBuilder: (context, index) {
              return widget.itemBuilder(widget.items[index]);
            },
          ),
        ),
        if (widget.isLoading && widget.items.isNotEmpty)
          (widget.loadingWidget ?? const CircularProgressIndicator()),
      ],
    );
  }
}

import 'package:flutter/material.dart';

import 'master_animated_list_view_item.dart';

class MasterAnimatedListView<T> extends StatefulWidget {
  final List<T> items;
  final Widget Function(BuildContext context, int index) itemBuilder;
  final Duration duration;
  final AnimationVariant animationVariant;
  final Curve animationCurve;

  /// Whether to animate the already animated list view item.
  final bool once;

  const MasterAnimatedListView({
    super.key,
    required this.items,
    required this.itemBuilder,
    this.duration = const Duration(milliseconds: 200),
    this.animationVariant = AnimationVariant.scale,
    this.once = false,
    this.animationCurve = Curves.ease,
  });

  @override
  State<MasterAnimatedListView<T>> createState() =>
      _MasterAnimatedListViewState<T>();
}

class _MasterAnimatedListViewState<T> extends State<MasterAnimatedListView<T>> {
  final _listItems = <T>[];
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();

  @override
  void initState() {
    _loadItems();
    super.initState();
  }

  void _loadItems() {
    // fetching data from web api, db...

    var future = Future(() {});
    for (var i = 0; i < widget.items.length; i++) {
      future = future.then((_) {
        return Future.delayed(widget.duration, () {
          _listItems.add(widget.items[i]);
          _listKey.currentState?.insertItem(_listItems.length - 1);
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedList(
      key: _listKey,
      padding: const EdgeInsets.only(top: 10),
      initialItemCount: _listItems.length,
      itemBuilder: (context, index, animation) {
        return MasterAnimatedListViewItem(
          duration: widget.duration,
          animationVariant: widget.animationVariant,
          once: widget.once,
          animaionCurve: widget.animationCurve,
          child: widget.itemBuilder(context, index),
        );
      },
    );
    // return ListView.builder(
    //   physics: const ScrollPhysics(),
    //   itemBuilder: (context, index) => MasterAnimatedListViewItem(
    //     duration: widget.duration,
    //     animationVariant: widget.animationVariant,
    //     once: widget.once,
    //     child: widget.itemBuilder(context, index),
    //   ),
    //   itemCount: widget.items.length,
    // );
  }
}
// void _unloadItems() {
//   var future = Future(() {});
//   for (var i = _listItems.length - 1; i >= 0; i--) {
//     future = future.then((_) {
//       return Future.delayed(const Duration(milliseconds: 100), () {
//         final deletedItem = _listItems.removeAt(i);
//         _listKey.currentState?.removeItem(i,
//                 (BuildContext context, Animation<double> animation) {
//               return SlideTransition(
//                 position: CurvedAnimation(
//                   curve: Curves.easeOut,
//                   parent: animation,
//                 ).drive((Tween<Offset>(
//                   begin: const Offset(1, 0),
//                   end: const Offset(0, 0),
//                 ))),
//                 child: deletedItem,
//               );
//             });
//       });
//     });
//   }
// }
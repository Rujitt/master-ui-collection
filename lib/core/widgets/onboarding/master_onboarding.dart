import 'package:flutter/material.dart';

class MasterOnboarding<T> extends StatefulWidget {
  final List<T> items;
  final Widget Function(BuildContext context, int index) itemBuilder;
  final bool showIndicators;
  final int initialPage;
  final Widget Function(
    BuildContext context,
    int index,
    bool isActive,
  )? indicatorBuilder;
  const MasterOnboarding({
    super.key,
    required this.items,
    required this.itemBuilder,
    this.showIndicators = true,
    this.initialPage = 0,
    this.indicatorBuilder,
  });

  @override
  State<MasterOnboarding<T>> createState() => _MasterOnboardingState<T>();
}

class _MasterOnboardingState<T> extends State<MasterOnboarding<T>> {
  late int _currentPageIndex;
  late PageController _controller;

  @override
  void initState() {
    super.initState();
    _currentPageIndex = widget.initialPage;
    _controller = PageController(initialPage: widget.initialPage);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: PageView.builder(
            controller: _controller,
            itemBuilder: widget.itemBuilder,
            itemCount: widget.items.length,
            onPageChanged: (value) {
              setState(() {
                _currentPageIndex = value;
              });
            },
          ),
        ),
        if (widget.showIndicators) _buildIndicators(context),
      ],
    );
  }

  Widget _buildIndicators(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ...List.generate(
            widget.items.length,
            (index) => GestureDetector(
              onTap: () {
                _controller.animateToPage(
                  index,
                  duration: const Duration(milliseconds: 300),
                  curve: Curves.bounceInOut,
                );
              },
              child: widget.indicatorBuilder != null
                  ? widget.indicatorBuilder!(
                      context,
                      index,
                      index == _currentPageIndex,
                    )
                  : AnimatedContainer(
                      margin: const EdgeInsets.symmetric(horizontal: 5),
                      width: _currentPageIndex == index ? 20 : 10,
                      height: 10,
                      decoration: BoxDecoration(
                        color: _currentPageIndex == index
                            ? Theme.of(context).primaryColor
                            : Colors.grey,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      duration: const Duration(milliseconds: 300),
                    ),
            ),
          )
        ],
      ),
    );
  }
}

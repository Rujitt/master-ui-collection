// import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';
//
// class MasterDatePicer extends StatefulWidget {
//   final ValueChanged<DateTime> onDateSelected;
//   final Color activeColor;
//   final Color textColor;
//   final double fontSize;
//   final double borderRadius;
//   final String dateFormat;
//
//   const MasterDatePicer(
//       {super.key,
//       required this.onDateSelected,
//       this.activeColor = Colors.white,
//       this.textColor = Colors.black,
//       this.fontSize = 16.0,
//       this.borderRadius = 10.0,
//       this.dateFormat = 'dd/MM/yyyy'});
//
//   @override
//   State<MasterDatePicer> createState() => _MasterDatePicerState();
// }
//
// class _MasterDatePicerState extends State<MasterDatePicer> {
//   late DateTime _selectedDate;
//
//   @override
//   void initState() {
//     super.initState();
//     _selectedDate = DateTime.now();
//   }
//
//   Future<void> _selectDate(BuildContext context) async {
//     final DateTime? picked = await showDatePicker(
//       context: context,
//       initialDate: _selectedDate,
//       firstDate: DateTime(2000),
//       lastDate: DateTime(2100),
//       builder: (context, child) => Theme(
//           data: ThemeData.light().copyWith(
//             colorScheme: ColorScheme.light(
//               primary: widget.textColor, // Text color
//               onPrimary: widget.activeColor, // Background color
//             ),
//           ),
//           child: child!),
//     );
//     if (picked != null && picked != _selectedDate) {
//       setState(() {
//         _selectedDate = picked;
//       });
//       widget.onDateSelected(_selectedDate);
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     final formattedDate = DateFormat(widget.dateFormat).format(_selectedDate);
//
//     return TextButton(
//       onPressed: () {
//         _selectDate(context);
//       },
//       child: Container(
//         decoration: BoxDecoration(
//           color: widget.activeColor,
//           borderRadius: BorderRadius.circular(widget.borderRadius),
//         ),
//         padding: const EdgeInsets.all(8.0),
//         child: Text(
//           formattedDate,
//           style: TextStyle(
//             fontSize: widget.fontSize,
//             color: widget.textColor,
//           ),
//         ),
//       ),
//     );
//   }
// }

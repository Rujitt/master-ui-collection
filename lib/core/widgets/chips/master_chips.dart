import 'package:flutter/material.dart';

class MasterChips extends StatelessWidget {
  final Widget? avatar;
  final Widget label;
  final TextStyle? labelStyle;
  final EdgeInsetsGeometry? labelPadding;
  final Widget? deleteIcon;
  final void Function()? onDeleted;
  final Color? deleteIconColor;
  final String? deleteButtonTooltipMessage;
  final OutlinedBorder? shape;
  final FocusNode? focusNode;
  final bool autofocus;
  final Color? backgroundColor;
  final EdgeInsetsGeometry? padding;
  final VisualDensity? visualDensity;
  final MaterialTapTargetSize? materialTapTargetSize;
  final double? elevation;
  final Color? shadowColor;
  final Color? surfaceTintColor;
  final IconThemeData? iconTheme;
  final bool useDeleteButtonTooltip;
  final VoidCallback? onPressed;
  final bool enableBorder;
  final double borderWidth;

  final Clip clipBehavior;
  const MasterChips(
      {super.key,
      this.avatar,
      required this.label,
      this.labelPadding,
      this.deleteIcon,
      this.onDeleted,
      this.deleteIconColor,
      this.deleteButtonTooltipMessage,
      this.shape,
      this.focusNode,
      this.backgroundColor,
      this.padding,
      this.visualDensity,
      this.materialTapTargetSize,
      this.elevation,
      this.shadowColor,
      this.surfaceTintColor,
      this.iconTheme,
      this.useDeleteButtonTooltip = true,
      this.labelStyle,
      this.autofocus = false,
      this.clipBehavior = Clip.none,
      this.onPressed,
      this.enableBorder = false,
      this.borderWidth = 2});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Chip(
        label: label,
        backgroundColor: backgroundColor,
        shape: shape,
        elevation: elevation,
        focusNode: focusNode,
        labelStyle: labelStyle,
        padding: const EdgeInsets.all(12),
        labelPadding: labelPadding,
        autofocus: autofocus,
        onDeleted: onDeleted,
        deleteIcon: deleteIcon,
        deleteIconColor: deleteIconColor,
        deleteButtonTooltipMessage: deleteButtonTooltipMessage,
        side: enableBorder ? BorderSide(width: borderWidth) : BorderSide.none,
        iconTheme: iconTheme,
        clipBehavior: clipBehavior,
        avatar: avatar,
        materialTapTargetSize: materialTapTargetSize,
        shadowColor: backgroundColor,
        surfaceTintColor: surfaceTintColor,
        visualDensity: visualDensity,
      ),
    );
  }
}

import 'package:dfunc/dfunc.dart';
import 'package:flutter/material.dart';

class MasterChipGroup<T> extends StatefulWidget {
  final List<T> items;
  final Widget Function(BuildContext context, T item,int index) labelBuilder;
  final Widget? Function(BuildContext context, T item,int index)? avatarBuilder;
  final void Function(T item)? onDeleted;
  final double spacing;
  final Widget? deleteIcon;
  final Color? backgroundColor;
  final Color? selectedColor;
  final Color? checkmarkColor;
  final bool multiSelect;

  const MasterChipGroup({
    super.key,
    required this.items,
    required this.labelBuilder,
    this.avatarBuilder,
    this.spacing = 16.0,
    this.deleteIcon,
    this.backgroundColor,
    this.selectedColor,
    this.checkmarkColor,
    this.onDeleted,
    this.multiSelect = false,
  });

  @override
  State<MasterChipGroup<T>> createState() => _MasterChipGroupState<T>();
}

class _MasterChipGroupState<T> extends State<MasterChipGroup<T>> {
  List<int> selectedIndexes = [];
  int? selectedIndex;

  void _onSelected(int index) {
    if (widget.multiSelect) {
      List<int> selectedIndexesCopy = [...selectedIndexes];
      selectedIndexesCopy.contains(index)
          ? selectedIndexesCopy.remove(index)
          : selectedIndexesCopy.add(index);
      setState(() => selectedIndexes = selectedIndexesCopy);
      return;
    }
    setState(() {
      selectedIndex = index == selectedIndex ? null : index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: widget.spacing,
      children: widget.items
          .mapIndexed(
            (index, item) => FilterChip(
              selected: widget.multiSelect
                  ? selectedIndexes.contains(index)
                  : selectedIndex == index,
              label: widget.labelBuilder(context, item,index),
              avatar: widget.avatarBuilder?.call(context, item,index),
              backgroundColor: widget.backgroundColor,
              selectedColor: widget.selectedColor,
              checkmarkColor: widget.checkmarkColor,
              onSelected: (_) => _onSelected(index),
            ),
          )
          .toList(),
    );
  }
}

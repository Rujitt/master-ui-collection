import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class MasterAvatarPicker extends StatefulWidget {
  final double radius;
  final Color backgroundColor;
  final Widget? icon;
  final double? minRadius;
  final double? maxRadius;
  final ImageProvider<Object>? defaultImage;
  final void Function(File)? onImagePicked;
  const MasterAvatarPicker({
    super.key,
    this.icon = const Icon(
      Icons.edit,
      color: Colors.white,
    ),
    this.backgroundColor = Colors.grey,
    this.radius = 30.0,
    this.defaultImage,
    this.onImagePicked,
    this.minRadius,
    this.maxRadius,
  });

  @override
  State<MasterAvatarPicker> createState() => _MasterAvatarPickerState();
}

class _MasterAvatarPickerState extends State<MasterAvatarPicker> {
  File? _imageFile;

  void _handlePickImage() async {
    ImagePicker picker = ImagePicker();
    XFile? image = await picker.pickImage(source: ImageSource.gallery);
    if (image == null) return;
    File imageFile = File(image.path);
    widget.onImagePicked?.call(imageFile);
    setState(() {
      _imageFile = imageFile;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _handlePickImage,
      child: CircleAvatar(
        backgroundColor: widget.backgroundColor,
        backgroundImage:
            _imageFile != null ? FileImage(_imageFile!) : widget.defaultImage,
        radius: widget.radius,
        minRadius: widget.minRadius,
        maxRadius: widget.maxRadius,
        child: Center(
          child: widget.icon,
        ),
      ),
    );
  }
}

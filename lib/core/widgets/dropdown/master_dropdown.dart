import 'package:flutter/material.dart';



class MasterDropdown<T> extends StatelessWidget {
  final List<T> items;
  final T? value;
  final String Function(T item) displayText;
  final void Function(T? newValue) onChanged;
  final double itemHeight;
  final Color? itemBackgroundColor;
  final double? borderRadius;
  final Widget? icon;
  final Decoration? decoration;


  const MasterDropdown({super.key, 
    required this.items,
    required this.value,
    required this.displayText,
    required this.onChanged,
    this.itemHeight = 40,
  this.itemBackgroundColor,
    this.borderRadius,
    this.icon ,
    this.decoration,
    
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: decoration,
      child: PopupMenuButton<T>(
        initialValue: value,
        color: Colors.lightGreen,
        tooltip: "",

        // icon: icon,

        itemBuilder: (BuildContext context) {
          return items.map((T item) {
            return PopupMenuItem<T>(
              value: item,
              height: itemHeight,
              child: Text(displayText(item)),
            );
          }).toList();
        },
        onSelected: onChanged,
        shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(borderRadius??4)),
        offset: const Offset(0, kToolbarHeight),
        child: icon??ListTile(
            title: Text(displayText(value as T)),
          trailing: const Icon(Icons.arrow_drop_down),
        ),
      ),
    );
  }
}
